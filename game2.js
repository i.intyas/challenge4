const hasil = document.getElementById("versus");

class Players {
    constructor(PlayerChoice, CompChoice) {
        this.PlayerChoice = PlayerChoice;
        this.CompChoice = CompChoice;
    }
    addPlayerChoice(pilihanOrang) {
        this.PlayerChoice[0]= pilihanOrang;
        
    }
    mesin() {
        this.choices = ["cgunting", "cbatu", "ckertas"];
        this.CompChoice[0] = this.choices[Math.floor((Math.random() * 3))];

        if (this.CompChoice[0] === "cgunting"){
            document.getElementById("cgunting").style.backgroundColor="rgba(255,255,255,.2)";
            document.getElementById("cbatu").style.backgroundColor="transparent";
            document.getElementById("ckertas").style.backgroundColor="transparent";
        }
        else if (this.CompChoice[0] === "cbatu"){
            document.getElementById("cbatu").style.backgroundColor="rgba(255,255,255,.2)";
            document.getElementById("cgunting").style.backgroundColor="transparent";
            document.getElementById("ckertas").style.backgroundColor="transparent";
        }
        else {
            document.getElementById("ckertas").style.backgroundColor="rgba(255,255,255,.2)";
            document.getElementById("cgunting").style.backgroundColor="transparent";
            document.getElementById("cbatu").style.backgroundColor="transparent";
    };
        
    }
    keputusan() {
        console.log(`pilihan kamu adalah  = ${this.PlayerChoice}`);
        console.log(`pilihan komputer adalah  = ${this.CompChoice}`)

        if ((this.PlayerChoice[0] === "pgunting" && this.CompChoice[0] === "ckertas") || (this.PlayerChoice[0] === "pbatu" && this.CompChoice[0] === "cgunting") || (this.PlayerChoice[0] === "pkertas" && this.CompChoice[0] === "cbatu")) {
            hasil.style.fontSize = "99%";
            hasil.style.color = "white"
            hasil.style.backgroundColor = "green";
            hasil.innerHTML = "PLAYER WIN"
        }
        else if ((this.PlayerChoice[0] === "pgunting" && this.CompChoice[0] === "cbatu") || (this.PlayerChoice[0] === "pbatu" && this.CompChoice[0] === "ckertas") || (this.PlayerChoice[0] === "pkertas" && this.CompChoice[0] === "cgunting")) {
            hasil.style.fontSize = "99%";
            hasil.style.color = "white"
            hasil.style.backgroundColor = "green";
            hasil.innerHTML = "COM WIN"
        }
        else {
            this.pemenang = "seri";
            hasil.style.fontSize = "100%";
            hasil.style.backgroundColor = "green";
            hasil.innerHTML = "SERI"
        }
        
    }
    pilihan() {
        const misalGunting = document.getElementById("pgunting")
        misalGunting.addEventListener("click", () => {
            misalBatu.style.backgroundColor = "transparent";
            misalKertas.style.backgroundColor = "transparent";
            misalGunting.style.backgroundColor = "rgba(255,255,255,.2)";
            const pilihanOrang = 'pgunting';
            this.addPlayerChoice(pilihanOrang);
            this.mesin();
            this.keputusan();
        });

        const misalBatu = document.getElementById("pbatu")
        misalBatu.addEventListener("click", () => {
            misalGunting.style.backgroundColor = "transparent";
            misalKertas.style.backgroundColor = "transparent";
            misalBatu.style.backgroundColor = "rgba(255,255,255,.2)";
            const pilihanOrang = 'pbatu';
            this.addPlayerChoice(pilihanOrang);
            this.mesin();
            this.keputusan();
        });

        const misalKertas = document.getElementById("pkertas")
        misalKertas.addEventListener("click", () => {
            misalBatu.style.backgroundColor = "transparent";
            misalGunting.style.backgroundColor = "transparent";
            misalKertas.style.backgroundColor = "rgba(255,255,255,.2)";
            const pilihanOrang = 'pkertas';
            this.addPlayerChoice(pilihanOrang);
            this.mesin();
            this.keputusan();
        });

        const ulang = document.getElementById("refresh")
        ulang.addEventListener("click", () => {
            misalBatu.style.backgroundColor = "transparent";
            misalGunting.style.backgroundColor = "transparent";
            misalKertas.style.backgroundColor = "transparent";
            document.getElementById("ckertas").style.backgroundColor="transparent";
            document.getElementById("cgunting").style.backgroundColor="transparent";
            document.getElementById("cbatu").style.backgroundColor="transparent";
            hasil.style.fontSize = "5rem";
            hasil.style.color = "red";
            hasil.style.backgroundColor = "transparent";
            hasil.innerHTML ="VS";
        });
    }
}

const jalan = new Players([],[]);
jalan.pilihan();
